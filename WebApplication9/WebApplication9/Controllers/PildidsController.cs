﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication9;

namespace WebApplication9.Controllers
{
    public class PildidsController : Controller
    {
        private OhooEntities db = new OhooEntities();

        // GET: Pildids
        public ActionResult Index()
        {
            return View(db.Pildid.ToList());
        }

        // GET: Pildids/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pildid pildid = db.Pildid.Find(id);
            if (pildid == null)
            {
                return HttpNotFound();
            }
            return View(pildid);
        }

        // GET: Pildids/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pildids/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "kood,nimi,pilt")] Pildid pildid)
        {
            if (ModelState.IsValid)
            {
                db.Pildid.Add(pildid);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pildid);
        }

        // GET: Pildids/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pildid pildid = db.Pildid.Find(id);
            if (pildid == null)
            {
                return HttpNotFound();
            }
            return View(pildid);
        }

        // POST: Pildids/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "kood,nimi,pilt")] Pildid pildid)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pildid).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pildid);
        }

        // GET: Pildids/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pildid pildid = db.Pildid.Find(id);
            if (pildid == null)
            {
                return HttpNotFound();
            }
            return View(pildid);
        }

        // POST: Pildids/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pildid pildid = db.Pildid.Find(id);
            db.Pildid.Remove(pildid);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
