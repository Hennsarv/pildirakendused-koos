﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp34
{
    class Program
    {
        static string Kaust = @"..\..\Pildid";
        static string KaustOne = @"C:\Users\sarvi\OneDrive\Pildid\Raamatud\Jänesepoeg kes luuletas";
        static void Main(string[] args)
        {
            DirectoryInfo di = null;
            while (true)
            {
                Console.Write("Mis teeme:\nimekiri (n), failid baasi (s), failid baasist (b): ");
                switch ((Console.ReadLine() + "x").ToLower().Substring(0, 1))
                {
                    case "x":
                        return;
                    case "n":
                        di = new DirectoryInfo(KaustOne);
                        foreach (var x in di.EnumerateFiles("*.jpg"))
                            Console.WriteLine(x.Name);

                        break;
                    case "s":
                        di = new DirectoryInfo(KaustOne);
                        using (OhooEntities oe = new OhooEntities())
                        {
                            foreach (var x in di.EnumerateFiles("*.jpg"))
                            {
                                oe.Pildid.Add(new Pildid { nimi = x.Name, pilt = File.ReadAllBytes(x.FullName) });
                                oe.SaveChanges();
                            }
                        }

                        break;
                    case "b":
                        using (OhooEntities oe = new OhooEntities())
                        {
                            foreach (var x in oe.Pildid)
                                File.WriteAllBytes(Kaust + "\\" + x.nimi, x.pilt);

                        }

                            break;
                    default:
                        Console.WriteLine("vale valik - vali n, s, b - lõpetamiseks Enter");
                        break;
                }
            }
          }
    }
}
